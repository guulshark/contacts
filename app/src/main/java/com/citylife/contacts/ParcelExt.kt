package com.citylife.contacts

import android.os.Parcel
import android.os.Parcelable

interface KParcelable : Parcelable {
    override fun describeContents() = 0
    override fun writeToParcel(parcel: Parcel, flags: Int)
}

inline fun <reified T> parcelableCreator(crossinline create: (Parcel) -> T) =
    object : Parcelable.Creator<T> {
        override fun createFromParcel(source: Parcel) = create(source)
        override fun newArray(size: Int) = arrayOfNulls<T>(size)
    }

inline fun <reified T> parcelableClassLoaderCreator(crossinline create: (Parcel, ClassLoader) -> T) =
    object : Parcelable.ClassLoaderCreator<T> {
        override fun createFromParcel(parcel: Parcel, loader: ClassLoader) = create(parcel, loader)

        override fun createFromParcel(parcel: Parcel) =
            createFromParcel(parcel, T::class.java.classLoader)

        override fun newArray(size: Int) = arrayOfNulls<T>(size)
    }

fun Parcel.readBoolean() = readByte() != 0.toByte()

fun Parcel.writeBoolean(value: Boolean) = writeByte(if (value) 1 else 0)

inline fun <reified T : Enum<T>> Parcel.readEnum() =
    readInt().let { if (it >= 0) enumValues<T>()[it] else null }

fun <T : Enum<T>> Parcel.writeEnum(value: T?) = writeInt(value?.ordinal ?: -1)

fun Parcel.writeNullableLong(value: Long?) = writeNullable(value) { writeLong(it) }

fun Parcel.readNullableLong() = readNullable { readLong() }

fun Parcel.writeNullableDouble(value: Double?) = writeNullable(value) { writeDouble(it) }

fun Parcel.readNullableDouble() = readNullable { readDouble() }

inline fun <T> Parcel.readNullable(reader: () -> T) = if (readInt() != 0) reader() else null

inline fun <T> Parcel.writeNullable(value: T?, writer: (T) -> Unit) {
    if (value != null) {
        writeInt(1)
        writer(value)
    } else {
        writeInt(0)
    }
}