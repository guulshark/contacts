package com.citylife.contacts.data

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations

@MainThread
fun <T> LiveData<T>.distinct(): LiveData<T> {
    val result = MediatorLiveData<T>()
    result.addSource(this) { if (it != result.value) result.value = it }
    return result
}

@MainThread
inline fun <T> LiveData<T>.distinct(crossinline observer: (data: T?) -> Unit): LiveData<T> {
    val result = MediatorLiveData<T>()
    result.addSource(this) {
        if (it != result.value) result.value = it
        observer(value)
    }
    return result
}

@MainThread
fun <T, R> LiveData<T>.map(transform: (T) -> R): LiveData<R> {
    return Transformations.map(this, transform)
}

@MainThread
fun <T, R> LiveData<T>.switchMap(transform: (T) -> LiveData<R>?): LiveData<R> {
    return Transformations.switchMap(this, transform)
}

@MainThread
inline fun <T> LiveData<T>.observe(owner: LifecycleOwner, crossinline observer: (T?) -> Unit) {
    observe(owner, Observer { observer(it) })
}

@MainThread
fun <T> LiveData<T>.addAsSourceTo(mediator: MediatorLiveData<*>, observer: (T) -> Unit) {
    mediator.addSource(this, observer)
}