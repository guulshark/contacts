package com.citylife.contacts.data

import androidx.lifecycle.LiveData
import javax.inject.Inject

class ContactsRepositoryImpl @Inject constructor(
    private val contactsApi: ContactsApi
) : ContactsRepository {

    override fun loadContacts(): LiveData<List<Contact>?> {
        return contactsApi.getUsers().map { response ->
            if (response.isSuccessful) {
                response.body() ?: emptyList()
            } else {
                emptyList()
            }
        }
    }
}