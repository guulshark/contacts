package com.citylife.contacts.data

import androidx.lifecycle.LiveData
import retrofit2.Response
import retrofit2.http.GET

interface ContactsApi {
    @GET("/users")
    fun getUsers(): LiveData<Response<List<Contact>?>>
}