package com.citylife.contacts.data

import android.os.Parcel
import com.citylife.contacts.KParcelable
import com.citylife.contacts.parcelableCreator

data class Contact(
    val id: Long,
    val name: String?,
    val username: String?,
    val email: String?,
    val address: Address,
    val phone: String?,
    val website: String?,
    val company: Company
) : KParcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        Address(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            GeoLocation(
                parcel.readString(),
                parcel.readString()
            )
        ),
        parcel.readString(),
        parcel.readString(),
        Company(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
        )
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeLong(id)
            writeString(name)
            writeString(username)
            writeString(email)
            writeString(address.suite)
            writeString(address.street)
            writeString(address.city)
            writeString(address.zipcode)
            writeString(address.geo.lat)
            writeString(address.geo.lng)
            writeString(phone)
            writeString(website)
            writeString(company.name)
            writeString(company.catchPhrase)
            writeString(company.bs)
        }
    }

    companion object {
        @JvmField
        val CREATOR = parcelableCreator(::Contact)
    }
}

data class Address(
    val suite: String?,
    val street: String?,
    val city: String?,
    val zipcode: String?,
    val geo: GeoLocation
)

data class GeoLocation(
    val lat: String?,
    val lng: String?
)

data class Company(
    val name: String?,
    val catchPhrase: String?,
    val bs: String?
)