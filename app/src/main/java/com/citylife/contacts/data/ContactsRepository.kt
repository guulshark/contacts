package com.citylife.contacts.data

import androidx.lifecycle.LiveData
import com.citylife.contacts.contacts.ContactEmail

interface ContactsRepository {
    fun loadContacts(): LiveData<List<Contact>?>
}