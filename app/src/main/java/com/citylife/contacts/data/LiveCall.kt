package com.citylife.contacts.data

import androidx.annotation.CallSuper
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A LiveData class encapsulates retrofit [Call] and implements retrofit [Callback].
 * [Call] enqueued when the number of active observers change to 1 from 0 and
 * response was not received yet. [Call] canceled when the number of active observers change
 * from 1 to 0 and response was not received yet. [Call] can be repeated via [repeat],
 * if there are active observers (or when they start observing).
 * @param R Successful response body type of [Call]
 * @param T The type of data held by this instance of [LiveData]
 */
abstract class LiveCall<R, T>(private var call: Call<R>) : LiveData<T>(), Callback<R> {
    private var isReceived = false

    @CallSuper
    override fun onActive() {
        if (!isReceived && (!call.isExecuted || call.isCanceled)) {
            if (call.isCanceled) call = call.clone()
            call.enqueue(this)
        }
    }

    @CallSuper
    override fun onInactive() {
        if (!isReceived && call.isExecuted && !call.isCanceled) call.cancel()
    }

    final override fun onResponse(call: Call<R>, response: Response<R>) {
        isReceived = true
        onResponse(response)
    }

    final override fun onFailure(call: Call<R>, t: Throwable) {
        isReceived = true
        onFailure(t)
    }

    /**
     * Repeat the [Call].
     * When there are active observers, this will enqueue a [Call] immediately.
     * Otherwise [Call] will be enqueued when the number of active observers change to 1 from 0.
     */
    @MainThread
    fun repeat() {
        if (call.isExecuted && !call.isCanceled) call.cancel()
        isReceived = false
        if (hasActiveObservers()) {
            if (call.isCanceled) call = call.clone()
            call.enqueue(this)
        }
    }

    abstract fun onResponse(response: Response<R>)

    abstract fun onFailure(t: Throwable)
}