package com.citylife.contacts.data

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<in T : Any> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(model: T): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(models: List<T>): LongArray

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(model: T): Int

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(models: List<T>): Int

    @Delete
    fun delete(model: T): Int

    @Delete
    fun delete(models: List<T>): Int
}
