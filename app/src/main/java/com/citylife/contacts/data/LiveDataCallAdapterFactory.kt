package com.citylife.contacts.data

import androidx.lifecycle.LiveData
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.CallAdapter.Factory
import retrofit2.Response
import retrofit2.Retrofit

class LiveDataCallAdapterFactory : Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (getRawType(returnType) != LiveData::class.java) {
            return null
        }
        if (returnType !is ParameterizedType) {
            throw IllegalArgumentException("LiveData return type must be parameterized")
        }
        val innerType = getParameterUpperBound(0, returnType)
        if (getRawType(innerType) == Response::class.java) {
            // Generic type is Response<T>.
            // Extract T and create the Response version of the adapter.
            if (innerType !is ParameterizedType) {
                throw IllegalStateException("Response must be parameterized")
            }
            val responseType = getParameterUpperBound(0, innerType)
            return ResponseCallAdapter<Any>(responseType)
        }
        return BodyCallAdapter<Any>(innerType)
    }

    private abstract class BaseCallAdapter<R, T>(private val responseType: Type) :
        CallAdapter<R, LiveData<T>> {

        override fun responseType() = responseType

        abstract override fun adapt(call: Call<R>): LiveData<T>
    }

    private class BodyCallAdapter<R>(responseType: Type) : BaseCallAdapter<R, R>(responseType) {

        override fun adapt(call: Call<R>): LiveData<R> {
            return object : LiveCall<R, R>(call) {
                override fun onResponse(response: Response<R>) {
                    postValue(response.body())
                }

                override fun onFailure(t: Throwable) {
                    postValue(null)
                }
            }
        }
    }

    private class ResponseCallAdapter<R>(responseType: Type) :
        BaseCallAdapter<R, Response<R>>(responseType) {

        override fun adapt(call: Call<R>): LiveData<Response<R>> {
            return object : LiveCall<R, Response<R>>(call) {
                override fun onResponse(response: Response<R>) {
                    postValue(response)
                }

                override fun onFailure(t: Throwable) {
                    val content = t.message ?: t.toString()
                    val body = ResponseBody.create(MediaType.parse("text/plain"), content)
                    postValue(Response.error<R>(599, body))
                }
            }
        }
    }
}