package com.citylife.contacts

interface OnBackPressListener {
    fun onBackPressed(): Boolean
}