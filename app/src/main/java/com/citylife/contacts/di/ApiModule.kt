package com.citylife.contacts.di

import android.content.Context
import com.citylife.contacts.BuildConfig
import com.citylife.contacts.data.ContactsApi
import com.citylife.contacts.data.LiveDataCallAdapterFactory
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
internal class ApiModule {

    @Singleton
    @Provides
    fun provideRetrofit(context: Context): Retrofit {
        val gson = GsonBuilder().create()
        val builder = OkHttpClient.Builder()
            .connectionPool(ConnectionPool(5, 60, TimeUnit.SECONDS))
            .cache(Cache(context.cacheDir, 5 * 1024 * 1024))
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(HttpLoggingInterceptor().setLevel(Level.BODY))
        }
        return Retrofit.Builder()
            .client(builder.build())
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
    }

    @Singleton
    @Provides
    fun provideContactsApi(retrofit: Retrofit): ContactsApi {
        return retrofit.create(ContactsApi::class.java)
    }
}
