package com.citylife.contacts.di

import com.citylife.contacts.HomeActivity
import com.citylife.contacts.HomeModule
import com.citylife.contacts.contactdetails.ContactDetailsModule
import com.citylife.contacts.contacts.ContactsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            HomeModule::class,
            ContactsModule::class,
            ContactDetailsModule::class
        ]
    )
    internal abstract fun contributeHomeActivity(): HomeActivity
}
