package com.citylife.contacts.di

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.citylife.contacts.data.ContactsRepository
import com.citylife.contacts.data.ContactsRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Defines all the classes that need to be provided in the scope of the app.
 */
@Module
interface AppModule {

    @Binds
    fun bindContext(app: Application): Context

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Singleton
    @Binds
    fun bindContactsRepository(contactsRepo: ContactsRepositoryImpl): ContactsRepository
}
