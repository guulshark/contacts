package com.citylife.contacts

import android.util.Log
import androidx.lifecycle.ViewModel
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val router: Router
) : ViewModel() {

    init {
        Log.d("HomeViewModel", "init")
        router.newRootScreen(Screens.Contacts)
    }

    internal fun action() {
        router.newRootScreen(Screens.Contacts)
    }
}