package com.citylife.contacts

import android.view.View
import androidx.recyclerview.widget.RecyclerView


abstract class ClickableViewHolder<VH : RecyclerView.ViewHolder>(
    itemView: View,
    private val onClickListener: OnViewHolderClickListener<VH>?
) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    init {
        @Suppress("LeakingThis")
        if (onClickListener != null) itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        @Suppress("UNCHECKED_CAST")
        onClickListener?.onViewHolderClick(this as VH)
    }

    interface OnViewHolderClickListener<VH : RecyclerView.ViewHolder> {
        fun onViewHolderClick(holder: VH)
    }
}