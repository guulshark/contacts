package com.citylife.contacts

import com.citylife.contacts.contactdetails.ContactDetailsFragment
import com.citylife.contacts.contacts.ContactsFragment
import com.citylife.contacts.data.Contact
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    object Contacts : SupportAppScreen() {
        override fun getFragment() = ContactsFragment()
    }

    data class ContactDetails(
        private val contact: Contact
    ) : SupportAppScreen() {
        override fun getFragment() = ContactDetailsFragment.create(contact)
    }
}