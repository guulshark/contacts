package com.citylife.contacts

import com.citylife.contacts.di.AppComponent
import com.citylife.contacts.di.DaggerAppComponent

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {
        appComponent = buildAppCompanent()
        return appComponent
    }

    private fun buildAppCompanent(): AppComponent {
        val appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent.inject(this)
        return appComponent
    }

    companion object {
        var appComponent: AppComponent? = null
            private set
    }
}
