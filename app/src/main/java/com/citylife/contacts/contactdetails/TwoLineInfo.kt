package com.citylife.contacts.contactdetails

data class TwoLineInfo(
    val title: String?,
    val subtitle: String?
)