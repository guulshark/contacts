package com.citylife.contacts.contactdetails

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.citylife.contacts.OnBackPressListener
import com.citylife.contacts.R
import com.citylife.contacts.data.Contact
import com.citylife.contacts.data.observe
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_contact_details.*
import java.lang.IllegalArgumentException
import javax.inject.Inject

/**
 * Fragment contains detailed information about user.
 */
class ContactDetailsFragment : Fragment(), OnBackPressListener {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ContactDetailsViewModel

    private lateinit var nameViewHolder: TwoLineViewHolder
    private lateinit var phoneViewHolder: TwoLineViewHolder
    private lateinit var addressViewHolder: TwoLineViewHolder
    private lateinit var websiteViewHolder: TwoLineViewHolder
    private lateinit var companyViewHolder: TwoLineViewHolder

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(ContactDetailsViewModel::class.java)
        val contact = arguments?.getParcelable<Contact>("contact")
            ?: throw IllegalArgumentException("Invalid contact")
        viewModel.setContact(contact)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_contact_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        initViewHolders()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewModel.contact.observe(viewLifecycleOwner) {
            toolbar.title = it?.name
            nameViewHolder.bindInfo(
                TwoLineInfo(
                    it?.username,
                    getString(R.string.contact_details_label_name)
                )
            )
            phoneViewHolder.bindInfo(
                TwoLineInfo(
                    it?.phone,
                    getString(R.string.contact_details_label_phone)
                )
            )
            addressViewHolder.bindInfo(
                TwoLineInfo(
                    it?.address?.run { "$suite, $street, $city, $zipcode" },
                    getString(R.string.contact_details_label_address)
                )
            )
            websiteViewHolder.bindInfo(
                TwoLineInfo(
                    it?.website,
                    getString(R.string.contact_details_label_website)
                )
            )
            companyViewHolder.bindInfo(
                TwoLineInfo(
                    it?.company?.run { "$name, $catchPhrase" },
                    getString(R.string.contact_details_label_company)
                )
            )
        }
    }

    override fun onBackPressed(): Boolean {
        return viewModel.clickOnBack()
    }

    private fun setupToolbar() {
        val upIndicator = TypedValue()
        if (toolbar.context.theme.resolveAttribute(R.attr.homeAsUpIndicator, upIndicator, true)) {
            toolbar.setNavigationIcon(upIndicator.resourceId)
        }
        toolbar.setNavigationOnClickListener { viewModel.clickOnBack() }
    }

    private fun initViewHolders() {
        nameViewHolder = TwoLineViewHolder(container_name)
        phoneViewHolder = TwoLineViewHolder(container_phone)
        addressViewHolder = TwoLineViewHolder(container_address)
        websiteViewHolder = TwoLineViewHolder(container_website)
        companyViewHolder = TwoLineViewHolder(container_company)
    }

    companion object {
        private const val TAG = "ContactDetailsFragment"
        fun create(contact: Contact) = ContactDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable("contact", contact)
            }
        }
    }
}