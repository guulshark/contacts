package com.citylife.contacts.contactdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.citylife.contacts.contacts.ContactEmail
import com.citylife.contacts.data.Contact
import com.citylife.contacts.data.ContactsRepository
import com.citylife.contacts.data.switchMap
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ContactDetailsViewModel @Inject constructor(
    private val router: Router
) : ViewModel() {

    private val _contact = MutableLiveData<Contact>()
    internal val contact: LiveData<Contact>
        get() = _contact

    internal fun setContact(contact: Contact) {
        if (_contact.value != contact) {
            _contact.value = contact
        }
    }

    internal fun clickOnBack(): Boolean {
        router.exit()
        return true
    }

    companion object {
        private const val TAG = "ContactDetailsViewModel"
    }
}