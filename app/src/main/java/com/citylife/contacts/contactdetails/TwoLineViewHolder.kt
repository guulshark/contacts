package com.citylife.contacts.contactdetails

import android.view.View
import android.widget.TextView
import com.citylife.contacts.ClickableViewHolder
import com.citylife.contacts.R


class TwoLineViewHolder internal constructor(
    itemView: View,
    clickListener: OnViewHolderClickListener<TwoLineViewHolder>? = null
) : ClickableViewHolder<TwoLineViewHolder>(itemView, clickListener), View.OnClickListener {
    internal var info: TwoLineInfo? = null
        private set
    private val nameView: TextView = itemView.findViewById(R.id.txt_title)
    private val emailView: TextView = itemView.findViewById(R.id.txt_subtitle)

    internal fun bindInfo(info: TwoLineInfo?) {
        this.info = info
        if (info == null) {
            nameView.text = null
            emailView.text = null
        } else {
            nameView.text = info.title
            emailView.text = info.subtitle
        }
    }
}
