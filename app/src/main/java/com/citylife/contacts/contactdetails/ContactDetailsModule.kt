package com.citylife.contacts.contactdetails

import androidx.lifecycle.ViewModel
import com.citylife.contacts.di.FragmentScoped
import com.citylife.contacts.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 * Module where classes needed to create the [ContactDetailsFragment] are defined.
 */
@Module
internal abstract class ContactDetailsModule {
    /**
     * Generates an AndroidInjector for the [ContactDetailsFragment] as a Dagger subcomponent.
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeContactDetailsFragment(): ContactDetailsFragment

    /**
     * The ViewModels are created by Dagger in a map.
     * Via the [ViewModelKey], we define that we want to get a [ContactDetailsViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(ContactDetailsViewModel::class)
    internal abstract fun bindContactDetailsViewModel(viewModel: ContactDetailsViewModel): ViewModel
}