package com.citylife.contacts.contacts

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.citylife.contacts.ClickableViewHolder
import com.citylife.contacts.R
import com.citylife.contacts.data.observe
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_contacts.*
import kotlinx.android.synthetic.main.layout_described_empty.*
import kotlinx.android.synthetic.main.layout_progress_empty.*
import javax.inject.Inject

/**
 * Fragment contains list of short information about users.
 * <p>
 * Created by Ivan Nikolaev on 14.09.17.
 */
class ContactsFragment : Fragment(),
    ClickableViewHolder.OnViewHolderClickListener<ContactEmailViewHolder> {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ContactsViewModel
    private lateinit var adapter: ContactsAdapter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(ContactsViewModel::class.java)
        adapter = ContactsAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupSwipeRefreshLayout()
        setupRecyclerView(recycler_view)
        adapter.registerAdapterDataObserver(contactsObserver)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewModel.loading.observe(viewLifecycleOwner) {
            val itemCount = adapter.itemCount
            updateEmptyView(itemCount, it ?: false)
            if (it == false && refresh_layout.isRefreshing) {
                refresh_layout.isRefreshing = false
            }
        }
//        viewModel.refreshing.observe(viewLifecycleOwner) {
//            LogUtils.dt(TAG, "onChanged(refreshing): $it")
//            val itemCount = adapter.itemCount
//            refresh_layout.isRefreshing = it == true && itemCount > 0
//        }
        viewModel.contacts.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
//        itemsViewModel.emptyState.observe(viewLifecycleOwner) {
//            LogUtils.dt(TAG, "onChange(emptyState): $it")
//            val query = itemsViewModel.query.value
//            updateTextView(txt_title, it?.title, query)
//            updateTextView(txt_description, it?.description, query)
//            updateTextView(btn_action, it?.action)
//            with(img_icon) {
//                setImageResource(it?.iconRes ?: 0)
//                visibility = if (it?.iconRes == null) View.GONE else View.VISIBLE
//            }
//        }
    }

    override fun onDestroyView() {
        adapter.unregisterAdapterDataObserver(contactsObserver)
        super.onDestroyView()
    }

    override fun onViewHolderClick(holder: ContactEmailViewHolder) {
        viewModel.clickOnContact(holder.contact)
    }

    private fun setupToolbar() {
        toolbar.setTitle(R.string.app_name)
        toolbar.inflateMenu(R.menu.sort)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_sort_az -> {
                    viewModel.changeSortOrder(true)
                    true
                }
                R.id.action_sort_za -> {
                    viewModel.changeSortOrder(false)
                    true
                }
                else -> false
            }
        }
    }

    private fun setupSwipeRefreshLayout() {
        refresh_layout.setOnRefreshListener { viewModel.refresh() }
        refresh_layout.setOnChildScrollUpCallback { _, _ -> canContentScrollUp() }
    }

    private fun canContentScrollUp(): Boolean = when {
        recycler_view.visibility == View.VISIBLE -> recycler_view.canScrollVertically(-1)
        scroll_view.visibility == View.VISIBLE -> scroll_view.canScrollVertically(-1)
        else -> false
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    private val contactsObserver = object : RecyclerView.AdapterDataObserver() {

        override fun onChanged() {
            updateEmptyView(adapter.itemCount, viewModel.loading.value ?: false)
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            updateEmptyView(adapter.itemCount, viewModel.loading.value ?: false)
        }

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            updateEmptyView(adapter.itemCount, viewModel.loading.value ?: false)
        }
    }

    fun updateEmptyView(itemCount: Int, loading: Boolean) {
        if (itemCount > 0) {
            scroll_view.visibility = View.GONE
            recycler_view.visibility = View.VISIBLE
        } else {
            recycler_view.visibility = View.GONE
            scroll_view.visibility = View.VISIBLE
            if (loading) {
                layout_described_empty.visibility = View.GONE
                layout_progress_empty.visibility = View.VISIBLE
            } else {
                layout_progress_empty.visibility = View.GONE
                layout_described_empty.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        private const val TAG = "ContactsFragment"
    }
}