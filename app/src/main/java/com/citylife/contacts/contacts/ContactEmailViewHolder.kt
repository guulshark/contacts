package com.citylife.contacts.contacts

import android.view.View
import android.widget.TextView
import com.citylife.contacts.ClickableViewHolder
import com.citylife.contacts.R


class ContactEmailViewHolder internal constructor(
    itemView: View,
    clickListener: OnViewHolderClickListener<ContactEmailViewHolder>?
) : ClickableViewHolder<ContactEmailViewHolder>(itemView, clickListener), View.OnClickListener {
    internal var contact: ContactEmail? = null
        private set
    private val nameView: TextView = itemView.findViewById(R.id.txt_title)
    private val emailView: TextView = itemView.findViewById(R.id.txt_subtitle)

    internal fun bindContact(contact: ContactEmail?) {
        this.contact = contact
        if (contact == null) {
            nameView.text = null
            emailView.text = null
        } else {
            nameView.text = contact.name
            emailView.text = contact.email
        }
    }
}
