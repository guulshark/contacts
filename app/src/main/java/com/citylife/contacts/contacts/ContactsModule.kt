package com.citylife.contacts.contacts

import androidx.lifecycle.ViewModel
import com.citylife.contacts.di.FragmentScoped
import com.citylife.contacts.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 * Module where classes needed to create the [ContactsFragment] are defined.
 */
@Module
internal abstract class ContactsModule {
    /**
     * Generates an AndroidInjector for the [ContactsFragment] as a Dagger subcomponent.
     */
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeContactsFragment(): ContactsFragment

    /**
     * The ViewModels are created by Dagger in a map.
     * Via the [ViewModelKey], we define that we want to get a [ContactsViewModel] class.
     */
    @Binds
    @IntoMap
    @ViewModelKey(ContactsViewModel::class)
    internal abstract fun bindContactsViewModel(viewModel: ContactsViewModel): ViewModel
}