package com.citylife.contacts.contacts

import com.citylife.contacts.Identifiable

data class ContactEmail(
    override val id: Long,
    val name: String,
    val email: String
) : Identifiable