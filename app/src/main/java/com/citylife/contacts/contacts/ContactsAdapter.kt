package com.citylife.contacts.contacts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.citylife.contacts.ClickableViewHolder.OnViewHolderClickListener
import com.citylife.contacts.R
import com.citylife.contacts.SimpleItemCallback

class ContactsAdapter(
    private val onClickListener: OnViewHolderClickListener<ContactEmailViewHolder>
) : ListAdapter<ContactEmail, ContactEmailViewHolder>(SimpleItemCallback<ContactEmail>()) {

    private var distanceUnit: String? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactEmailViewHolder {
        return ContactEmailViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_two_line_info, parent, false),
            onClickListener
        )
    }

    override fun onBindViewHolder(holder: ContactEmailViewHolder, position: Int) {
        holder.bindContact(getItem(position))
    }

    override fun getItemId(position: Int): Long = getItem(position).id
}