package com.citylife.contacts.contacts

import android.util.Log
import androidx.lifecycle.*
import com.citylife.contacts.Screens
import com.citylife.contacts.data.Contact
import com.citylife.contacts.data.ContactsRepository
import ru.terrakok.cicerone.Router
import javax.inject.Inject

internal class ContactsViewModel @Inject constructor(
    private val contactsRepo: ContactsRepository,
    private val router: Router
) : ViewModel() {
    private val sortFromAtoZ = MutableLiveData<Boolean>()
    private var fullContacts = emptyList<Contact>()

    private val _loading = MutableLiveData<Boolean>()
    internal val loading: LiveData<Boolean>
        get() = _loading

    private val _contacts = MediatorLiveData<List<ContactEmail>>()
    internal val contacts: LiveData<List<ContactEmail>>
        get() = _contacts

    init {
        sortFromAtoZ.value = true
        _contacts.addSource(sortFromAtoZ) { isAZ ->
            _contacts.value?.let { contactEmails ->
                if (contactEmails.isNotEmpty()) {
                    _contacts.value = if (isAZ) {
                        contactEmails.sortedBy { it.name }
                    } else {
                        contactEmails.sortedByDescending { it.name }
                    }
                }
            }
        }
        refresh()
    }


    fun changeSortOrder(isFromAtoZ: Boolean) {
        if (sortFromAtoZ.value != isFromAtoZ) {
            sortFromAtoZ.value = isFromAtoZ
        }
    }

    fun refresh() {
        Log.d(TAG, "refresh")
        val source = contactsRepo.loadContacts()
        _loading.value = true
        _contacts.addSource(source) { contacts ->
            _contacts.removeSource(source)
            Log.d(TAG, "refresh: loadContacts.onChange - $contacts")
            _loading.value = false
            if (fullContacts != contacts) {
                fullContacts = contacts ?: emptyList()
                val contactEmails = fullContacts.map {
                    ContactEmail(
                        it.id,
                        it.name ?: "",
                        it.email ?: ""
                    )
                }
                _contacts.value = if (sortFromAtoZ.value == false) {
                    contactEmails.sortedByDescending { it.name }
                } else {
                    contactEmails.sortedBy { it.name }
                }
            }
        }
    }

    internal fun clickOnContact(contact: ContactEmail?) {
        contact?.let {
            fullContacts.find { it.id == contact.id }?.let { contact ->
                router.navigateTo(Screens.ContactDetails(contact))
            }
        }
    }

    companion object {
        private const val TAG = "ContactsViewModel"
    }
}