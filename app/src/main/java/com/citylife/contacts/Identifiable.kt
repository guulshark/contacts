package com.citylife.contacts

interface Identifiable {
    val id: Any
}